package cn.caijiajia.tp320.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;


@ControllerAdvice(assignableTypes = { ViewController.class })
public class ViewExceptionHandler extends cn.caijiajia.web.controller.ViewExceptionHandler {
}
