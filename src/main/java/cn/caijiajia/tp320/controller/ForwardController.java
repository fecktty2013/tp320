package cn.caijiajia.tp320.controller;

import cn.caijiajia.framework.threadlocal.ParameterThreadLocal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


@RestController("tp320ForwardController")
public class ForwardController extends cn.caijiajia.innersso.client.ForwardController {

    @Autowired
    private String feresearchUrl;

    @RequestMapping(value = "/feresearch/**")
    public void feresearch(HttpServletRequest request, HttpServletResponse response) {
        this.xforward(request, response, feresearchUrl + this.getRemains(request));
    }

    @RequestMapping(value = "/innersso-client/api/**")
    public void movies(HttpServletRequest request, HttpServletResponse response) {
        String redirectUrl = "https://api.douban.com" + this.getRemains(request);
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException ex) {
        }
        this.xforward(request, response, redirectUrl);
    }
}
