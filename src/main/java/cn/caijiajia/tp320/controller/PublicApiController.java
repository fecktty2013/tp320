package cn.caijiajia.tp320.controller;

import cn.caijiajia.web.vo.RestOK;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.*;

class User implements Serializable {
    private String id;

    private String name;

    private Integer age;

    private String eyeColor;

    private String address;

    private Date updatedAtDateTime;

    public User() {
        super();
    }

    public User(String id, String name, Integer age, String eyeColor, String address, Date updatedAtDateTime) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.eyeColor = eyeColor;
        this.address = address;
        this.updatedAtDateTime = updatedAtDateTime;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getUpdatedAtDateTime() {
        return updatedAtDateTime;
    }

    public void setUpdatedAtDateTime(Date updatedAtDateTime) {
        this.updatedAtDateTime = updatedAtDateTime;
    }
}

@RestController
public class PublicApiController {

    static List<User> userList = Lists.newArrayList(
        new User("1", "111", 111, "111", "111", new Date()),
        new User("2", "222", 222, "222", "222", new Date()),
        new User("3", "333", 333, "333", "333", new Date())
    );


    @RequestMapping(value = "/innersso-client/api/users", method = RequestMethod.GET)
    public Map<String, Object> users(HttpServletRequest request, HttpServletResponse response) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("users", userList.toArray());
        return data;
    }

    @RequestMapping(value = "/innersso-client/api/users", method = RequestMethod.POST)
    public RestOK createUser(@RequestBody User user) {
        user.setId(generateId());
        user.setUpdatedAtDateTime(new Date());
        userList.add(user);
        return new RestOK();
    }

    @RequestMapping(value = "/innersso-client/api/users/{userId}", method = RequestMethod.PUT)
    public RestOK updateUser(@PathVariable("userId") String userId, @RequestBody User user) {
        User selectUser = findUser(userId);
        selectUser.setAddress(user.getAddress());
        selectUser.setAge(user.getAge());
        selectUser.setEyeColor(user.getEyeColor());
        selectUser.setName(user.getName());
        selectUser.setUpdatedAtDateTime(new Date());
        return  new RestOK();
    }

    @RequestMapping(value = "/innersso-client/api/users/{userId}", method = RequestMethod.GET)
    public Map<String, Object> user(@PathVariable("userId") String userId) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("user", this.findUser(userId));
        return data;
    }

    private User findUser(String userId) {
        for(User user : userList) {
            if(user.getId().equals(userId)) {
                return user;
            }
        }
        return null;
    }

    private String generateId() {
        Integer id = 0;
        for(User user : userList) {
            if(Integer.parseInt(user.getId()) > id) {
                id = Integer.parseInt(user.getId());
            }
        }
        return String.valueOf(id + 1);
    }
}
