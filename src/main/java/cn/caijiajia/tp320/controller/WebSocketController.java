package cn.caijiajia.tp320.controller;

import cn.caijiajia.innersso.client.stomp.StompUser;
import cn.caijiajia.innersso.client.stomp.StompUserManager;
import cn.caijiajia.web.model.SkipApiNameAspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@SkipApiNameAspect
@EnableScheduling
@Controller
public class WebSocketController {

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private StompUserManager manager;

    @MessageMapping("/chat")
    @SendTo("/topic/chat")
    public String chat(Principal principal, @Payload String message) {
        StompUser user = (StompUser) principal;
        return "[" + user.getUsername() + "]: " + message;
    }
}
