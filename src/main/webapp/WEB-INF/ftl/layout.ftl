<#macro html></#macro>

<#macro body></#macro>

<#macro title></#macro>

<#macro vcss></#macro>

<#macro css></#macro>

<#macro content></#macro>

<#macro vjs></#macro>

<#macro js></#macro>

<#macro tracking>
<#include "partials/tracking.ftl">
</#macro>

<#macro page>
<!DOCTYPE html>
<html <@html />>
<head>
  <title><@title /></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1" />
  <meta name="format-detection" content="telephone=no" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

  <link rel="icon" href="${contextPath}rsrc/img/favicon.png" />

  <#include "web/partials/head.ftl" />
  <#include "web/partials/tracking.ftl" />

  <@vcss />
  <@css />

</head>
<body <@body />>

<@content />
<@vjs />
<@js />
<@tracking />

</body>
</html>
</#macro>
