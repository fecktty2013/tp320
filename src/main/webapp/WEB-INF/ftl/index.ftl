<#include "layout.ftl" />

<#import "/web/macros/rsrc.ftl" as rsrc />

<#macro title>摩卡平台 - tp320</#macro>

<#macro vcss>
<@rsrc.css filename="vendor/antd-2.12.6.css" inline="false" />
</#macro>

<#macro css>
<@rsrc.dist filename="index-stamp4hash.css" inline="false" />
</#macro>

<#macro content>
<div class="App"></div>
</#macro>

<#macro js>
<script src="${contextPath}rsrc/innersso-client/shuhe.js"></script>

<#if env != "local">
<@rsrc.js filename="core-d74c7.js" inline="false" />
</#if>

<@rsrc.dist filename="index-stamp4hash.js" inline="false" />
</#macro>

<@page />
