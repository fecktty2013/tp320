const { clean, copy, replace, check } = require('@lattebank/build/gulp.config.js');
const gulp = require('gulp');


gulp.task('_clean', clean);


gulp.task('_copy', copy);


gulp.task('_replace', replace);

gulp.task('_check', check);

gulp.task('default', ['_clean'], function (done) {
  gulp.start('_copy');
  done();
});
