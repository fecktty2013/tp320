import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from '@lattebank/moka';
import { Input, List, Card, Row, Col } from 'antd';

@connect(state => ({
  websocket: state.websocket,
}))
export default class IndexPage extends Component {
  static propTypes = {
    websocket: PropTypes.object,    // eslint-disable-line
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.sendMsg = this.sendMsg.bind(this);
    this.setMsgInputRef = (element) => {
      this.msgInput = element;
    };
    this.state = { msgValue: '' };
  }

  handleChange(e) {
    this.setState({ msgValue: e.target.value });
  }

  async sendMsg(e) {
    if (!e.target.value) {
      return;
    }
    await this.props.dispatch({ type: 'websocket/sendMsg', payload: { msg: e.target.value } });
    this.setState({ msgValue: '' });
  }

  render() {
    const { websocket } = this.props;
    const msgList = websocket.get('msgList').toJS().map(m => m.message);

    return (
      <div style={{ background: '#ECECEC', padding: '30px' }}>
        <Row gutter={16}>
          <Col span={16}>
            <Card title="Card title" bordered={false}>
              <List
                bordered
                dataSource={msgList}
                renderItem={item => (<List.Item>{item}</List.Item>)} />
            </Card>
          </Col>
          <Col span={8}>
            <Card title="Card title" bordered={false}>
              <Input.TextArea
                autosize
                value={this.state.msgValue || ''}
                ref={this.setMsgInputRef}
                onChange={this.handleChange}
                onPressEnter={this.sendMsg} />
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
