import Immutable from 'immutable';
import $stomp from '@lattebank/webadmin-stomp';

export default {
  namespace: 'websocket',

  state: Immutable.fromJS({
    msgList: [],
  }),

  subscriptions: {
    setup({ history }) {  // eslint-disable-line
    },
    setupWebSocket({ dispatch }) {
      // TODO workaround for a bug in $stomp
      head.sso = head.sso || {};
      head.sso.dispatch = () => null;

      $stomp.config({ heartbeat: { outgoing: -1 } });
      dispatch({ type: 'regWebsocketChannel' });
      return () => dispatch({ type: 'unregWebsocketChannel' });
    },
  },

  effects: {
    *regWebsocketChannel({ payload }, { put, call, take }) {  // eslint-disable-line
      const chatChannel = yield call($stomp.eventChannel, 'wsdemo', '/topic/chat');
      while (true) { // eslint-disable-line
        const chatMsg = yield take(chatChannel);
        yield put({ type: 'newMsg', payload: { msg: chatMsg } });
      }
    },
    *unregWebsocketChannel({ payload }, { call }) {
      yield call($stomp.unEventChannel, 'wsdemo');
    },
    *sendMsg({ payload: { msg } }) {
      // yield call($stomp.send, 'wsdemo/msg', msg);
      yield $stomp.send('/chat', msg);
    },
  },

  reducers: {
    newMsg(state, action) {
      const newMsgs = [...(state.get('msgList') || []), action.payload.msg];
      return state.update('msgList', () => Immutable.fromJS(newMsgs));
    },
  },
};
