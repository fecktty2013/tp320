import Immutable from 'immutable';
import { routerRedux } from '@lattebank/moka/router';

export default {
  namespace: 'auth-login',

  state: Immutable.fromJS({
  }),

  subscriptions: {
    setup({ history, dispatch }) {  // eslint-disable-line
      return history.listenBefore((location) => {
        if (location.pathname.indexOf('invalid') >= 0) {
          dispatch(routerRedux.push('/users'));
        }
      });
    },
  },

  effects: {
    *dummyEffect({ payload }, { put, call }) {  // eslint-disable-line
    },
  },

  reducers: {
    dummyReducer(state) {
      return state;
    },
  },
};
