import { routerRedux } from '@lattebank/moka/router';

export default {
  namespace: 'bridge-warning',

  state: {
    title: 'title from model',
  },

  subscriptions: {
    setup({ history, dispatch }) {  // eslint-disable-line
    },
  },

  effects: {
    *navigateTo({ payload: { pathname, query } }, { put }) {
      yield put(routerRedux.push({ pathname, query }));
    },
  },

  reducers: {
  },
};
