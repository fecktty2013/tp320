import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from '@lattebank/moka';
import { bridgeSet, withBridge } from '@lattebank/moka/plugin/bridge';
import { Button } from 'antd';

@connect(state => ({
  title: state['bridge-warning'].title,
}))
@withBridge
export default class IndexPage extends Component {
  static propTypes = {
    title: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.dispatch(bridgeSet('title', 'Hey, I am the new title'));
  }

  render() {
    return (
      <div>
        <h2>{this.props.title}</h2>
        <Button type="primary" onClick={this.onClick}>点击更新</Button>
      </div>
    );
  }
}
