import Immutable from 'immutable';

export default {
  namespace: 'app',

  state: Immutable.fromJS({
    loading: false,
  }),

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  reducers: {
    loadStart(state) {
      return state.update('loading', () => true);
    },
    loadSuccess(state) {
      return state.update('loading', () => false);
    },
  },
};
