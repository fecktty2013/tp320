import Immutable from 'immutable';
import { routerRedux } from '@lattebank/moka/router';

export default {
  namespace: 'router-redux',

  state: Immutable.fromJS({
  }),

  subscriptions: {
    setup({ history, dispatch }) {  // eslint-disable-line
      setTimeout(() => {
        dispatch(routerRedux.push('/users'));
      }, 10000);
    },
  },

  effects: {
    *navigateTo({ payload: { pathname, query } }, { put }) {
      yield put(routerRedux.push({ pathname, query }));
    },
  },

  reducers: {
  },
};
