import React, { Component } from 'react';
import { connect } from '@lattebank/moka';
import { Button } from 'antd';

@connect(() => ({
}))
export default class IndexPage extends Component {
  static propTypes = {
  };

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.dispatch({ type: 'router-redux/navigateTo', payload: { pathname: '/users' } });
  }

  render() {
    return (
      <div>
        <h2>routerRedux</h2>
        <Button type="primary" onClick={this.onClick}>Click here to navigate</Button>
      </div>
    );
  }
}
