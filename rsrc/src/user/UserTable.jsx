import React from 'react';
import PropTypes from 'prop-types';
import ts2iso from 'vanilla.js/date/ts2iso';
import Spreadsheet from '@lattebank/spreadsheet';
import { Link } from '@lattebank/moka/router';
import { Icon } from 'antd';

export default function UserTable(props) {
  const { query } = props;
  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      search: 'string',
      key: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      search: 'string',
      key: 'name',
    },
    {
      title: 'Age',
      dataIndex: 'age',
      sorter: true,
      key: 'age',
    },
    {
      title: 'Eye Color',
      dataIndex: 'eyeColor',
      search: 'radio',
      key: 'eyeColor',
    },
    {
      title: '地址',
      dataIndex: 'address',
      search: 'string',
      sorter: true,
      key: 'address',
    },
    {
      title: 'Updated At',
      dataIndex: 'updatedAtDateTime',
      key: 'updatedAtDateTime',
      computed: record => ts2iso(record.updatedAtDateTime),
    },
    {
      title: <Link onlyActiveOnIndex to={{ pathname: '/users/-', query }} className="ant-btn ant-btn-sm ant-btn-primary">创建</Link>,
      key: 'operation',
      render: (text, record) => (
        <Link
          onlyActiveOnIndex
          to={{ pathname: `/users/${record.id}`, query }}
          className="ant-btn ant-btn-sm xor"
          activeClassName="ant-btn-link truth"
        >
          <Icon type="folder-open" className="xor-target truth-target" />
          <bdi className="xor-target">编辑</bdi>
        </Link>
      ),
    },
  ];

  return <Spreadsheet columns={columns} rowKey="id" {...props} />;
}

UserTable.propTypes = {
  query: PropTypes.object,  // eslint-disable-line
};
