import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from '@lattebank/moka';
import Immutable from 'immutable';
import { Breadcrumb } from 'antd';
import getPreserve from '@lattebank/spreadsheet/dist/preserve/arena.common';
import UserTable from '../UserTable';
import UserForm from './UserForm';

@connect(store => ({
  app: store.app,
  user: store.user,
}))
export default class UserEditorPage extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
  };

  static propTypes = {
    user: PropTypes.instanceOf(Immutable.Map),
  };

  constructor(props) {
    super(props);

    this.state = {
      error: null,
    };

    this.preserve = getPreserve().bind(this);
    this.save = this.save.bind(this);
  }

  preserve() { console.log(this); }

  async save(form) {
    this.setState({ error: null });

    if (form.id) {
      try {
        await this.props.dispatch({ type: 'user/updateUser', payload: { form } });
        this.context.router.push({ pathname: '/users', query: this.props.location.query });
      } catch (err) {
        this.setState({ error: err.message });
      }
    } else {
      try {
        await this.props.dispatch({ type: 'user/createUser', payload: { form } });
        this.context.router.push({ pathname: '/users' });
      } catch (err) {
        this.setState({ error: err.message });
      }
    }
  }

  render() {
    const { user, app, location: { query } } = this.props;
    const userIndex = user.get('userIndex');
    const userForm = user.get('userEdit');
    const loading = app.get('loading');

    const { error } = this.state;

    return (
      <div>
        <div className="ant-layout-breadcrumb">
          <Breadcrumb>
            <Breadcrumb.Item>TP301</Breadcrumb.Item>
            <Breadcrumb.Item>用户列表</Breadcrumb.Item>
            <Breadcrumb.Item>{this.props.params.userId}</Breadcrumb.Item>
            <Breadcrumb.Item>编辑</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <div className="ant-layout-container">
          <UserTable dataSource={userIndex} query={query} preserve={this.preserve} loading={loading} />
          <UserForm defaults={userForm} error={error} loading={loading} submit={this.save} />
        </div>
      </div>
    );
  }
}
