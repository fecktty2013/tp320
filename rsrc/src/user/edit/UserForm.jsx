import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import { Spin, Modal, Form, Input, Select } from 'antd';


const FormItem = Form.Item;
const Option = Select.Option;


const EYE_COLOR_OPTIONS = [
  { value: 'red', label: '红色' },
  { value: 'green', label: '绿色' },
  { value: 'blue', label: '蓝色' },
];


class UserForm extends Component {
  static propTypes = {
    defaults: PropTypes.instanceOf(Immutable.Map),
    loading: PropTypes.bool,
    error: PropTypes.string,
    submit: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.submit = this.submit.bind(this);
  }

  submit() {
    const defaults = this.props.defaults.toJS();
    const form = this.props.form.getFieldsValue();

    const command = {
      id: defaults.id,
      name: form.name,
      age: form.age,
      eyeColor: form.eyeColor,
      address: form.address,
    };

    this.props.submit(command);
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 14 },
      },
    };

    const { getFieldDecorator } = this.props.form;

    const { loading, error } = this.props;
    const defaults = this.props.defaults.toJS();

    return (
      <Modal title={defaults.id ? '修改' : '创建'} visible onOk={this.submit} okText="提交" onCancel={() => history.go(-1)}>
        <Spin spinning={loading}>
          <Form>
            <FormItem {...formItemLayout} label="Name">
              {getFieldDecorator('name', { initialValue: defaults.name, rules: [{ required: true, message: 'Name 必填', whitespace: true }] })(<Input />)}
            </FormItem>
            <FormItem {...formItemLayout} label="Age">
              {getFieldDecorator('age', { initialValue: defaults.age, rules: [{ required: true, message: 'Age 必填', whitespace: true }] })(<Input type="number" />)}
            </FormItem>
            <FormItem {...formItemLayout} label="Eye Color">
              {getFieldDecorator('eyeColor', { initialValue: defaults.eyeColor, rules: [{ required: true, message: 'Eye Color 必填', whitespace: true }] })(
                <Select showSearch>
                  {EYE_COLOR_OPTIONS.map(t => <Option value={t.value} key={t.value}>{t.value} - {t.label}</Option>)}
                </Select>)}
            </FormItem>
            <FormItem {...formItemLayout} label="地址">
              {getFieldDecorator('address', { initialValue: defaults.address, rules: [{ required: true, message: 'Address 必填', whitespace: true }] })(<Input />)}
            </FormItem>
            {error && <div className="ant-row ant-form-item has-error"><div className="ant-col-xs-offset-6 ant-col-xs-18 ant-form-explain">{error}</div></div>}
          </Form>
        </Spin>
      </Modal>
    );
  }
}


const Wrapped = Form.create()(UserForm);


export default Wrapped;

