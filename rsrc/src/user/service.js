import { $http } from '@lattebank/moka';

export function indexUser() {
  return $http.get(`${head.CONTEXT_PATH}innersso-client/api/users`);
}

export function showUser(userId) {
  return $http.get(`${head.CONTEXT_PATH}innersso-client/api/users/${userId}`);
}

export function createUser(form) {
  return $http.post(`${head.CONTEXT_PATH}innersso-client/api/users`, form);
}

export function updateUser(form) {
  return $http.put(`${head.CONTEXT_PATH}innersso-client/api/users/${form.id}`, form);
}
