import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from '@lattebank/moka';
import Immutable from 'immutable';
import { Breadcrumb } from 'antd';
import getPreserve from '@lattebank/spreadsheet/dist/preserve/arena.common';
import UserTable from './UserTable';


@connect(state => ({
  app: state.app,
  user: state.user,
}))
export default class UserIndexPage extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired,
  };

  static propTypes = {
    user: PropTypes.instanceOf(Immutable.Map),
  };

  constructor(props) {
    super(props);

    this.preserve = getPreserve().bind(this);
  }

  preserve() { console.log(this); }

  render() {
    const { app, user, location: { query } } = this.props;
    const loading = app.get('loading');
    const userIndex = user.get('userIndex');

    return (
      <div>
        <div className="ant-layout-breadcrumb">
          <Breadcrumb>
            <Breadcrumb.Item>TP320</Breadcrumb.Item>
            <Breadcrumb.Item>用户列表</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <div className="ant-layout-container">
          <UserTable dataSource={userIndex} query={query} preserve={this.preserve} loading={loading} />
        </div>
      </div>
    );
  }
}
