import Immutable from 'immutable';
import * as userService from './service';

export default {
  namespace: 'user',

  state: Immutable.fromJS({
    userIndex: [],
    userTotal: 0,
    userEdit: {},
  }),

  subscriptions: {
    setup({ history }) {  // eslint-disable-line
      return history.mapRouteToAction([
        { path: '/users', action: 'userPage' },
        { path: '/users/:userId', action: 'userEditPage' },
      ]);
    },
  },

  effects: {
    *userPage({ payload }, { call, put }) {  // eslint-disable-line
      yield put({ type: 'app/loadStart' });
      const { data } = yield call(userService.indexUser);
      yield put({ type: 'indexUserSuccess', payload: data });
      yield put({ type: 'app/loadSuccess' });
    },
    *userEditPage({ payload: { params } }, { put }) {  // eslint-disable-line
      yield put({ type: 'indexUser' });
      yield put({ type: 'showUser', payload: params.userId });
    },
    *indexUser({ payload }, { put, call }) {  // eslint-disable-line
      yield put({ type: 'app/loadStart' });
      const { data } = yield call(userService.indexUser);
      yield put({ type: 'indexUserSuccess', payload: data });
      yield put({ type: 'app/loadSuccess' });
    },
    *showUser({ payload: userId }, { put, call }) {  // eslint-disable-line
      if (userId === '-') {
        yield put({ type: 'showUserSuccess', payload: { user: {} } });
      } else {
        yield put({ type: 'app/loadStart' });
        const { data } = yield call(userService.showUser, userId);
        yield put({ type: 'showUserSuccess', payload: data });
        yield put({ type: 'app/loadSuccess' });
      }
    },
    *createUser({ payload: { form } }, { put, call }) {  // eslint-disable-line
      yield put({ type: 'app/loadStart' });
      yield call(userService.createUser, form);
      yield put({ type: 'app/loadSuccess' });
    },
    *updateUser({ payload: { form } }, { put, call }) {  // eslint-disable-line
      yield put({ type: 'app/loadStart' });
      yield call(userService.updateUser, form);
      yield put({ type: 'app/loadSuccess' });
    },
  },

  reducers: {
    indexUserSuccess(state, action) {
      return state.update('userIndex', () => Immutable.fromJS(action.payload.users));
    },
    showUserSuccess(state, action) {
      return state.update('userEdit', () => Immutable.fromJS(action.payload.user));
    },
  },
};
