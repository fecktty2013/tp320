import React, { Component } from 'react';


export default class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { children } = this.props;

    return (
      <div className="ant-layout-wrapper">
        {children}
      </div>
    );
  }
}
