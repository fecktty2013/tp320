import moka from '@lattebank/moka';
import createLoading from 'dva-loading';
import createBridge from '@lattebank/moka/plugin/bridge';
import app from './app';
import './style.scss';

moka({
  defaultRoute: '/users',
  defaultModule: app,
})
.use(createLoading())
.use(createBridge({ initialState: { title: 'This is the default title' } }))
.modules([
  {
    route: '/users',
    module: () => import(/* webpackChunkName: "user" */ './user'),
  },
  {
    route: '/users/:userId',
    module: () => import(/* webpackChunkName: "userEdit" */ './user/edit'),
  },
  {
    route: '/router-redux',
    module: () => import('./router-redux'),
  },
  {
    route: '/recursive-route',
    module: () => import('./recursive-route'),
    children: [
      {
        route: ':childId',
        module: () => import('./recursive-route/child'),
      },
    ],
  },
  {
    route: '/auth-login/:loginStatus',
    module: () => import('./auth-login'),
  },
  {
    route: '/websocket',
    module: () => import('./websocket'),
  },
  {
    route: '/movies',
    module: () => import('./movie'),
  },
  {
    route: '/bridge',
    module: () => import('./bridge'),
  },
  {
    route: '/bridge2',
    module: () => import('./bridge2'),
  },
  {
    route: '/bridge-warning',
    module: () => import('./bridge-warning'),
  },
])
.start('.App');
