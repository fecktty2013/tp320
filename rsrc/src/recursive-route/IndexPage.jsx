import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from '@lattebank/moka';

@connect(() => ({
}))
export default class IndexPage extends Component {
  static propTypes = {
    children: PropTypes.any,    // eslint-disable-line
  };

  render() {
    return (
      <div>
        <h2>Here is the common area among children pages</h2>
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}
