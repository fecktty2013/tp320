import Immutable from 'immutable';

export default {
  namespace: 'recursive-route',

  state: Immutable.fromJS({
  }),

  subscriptions: {
    setup({ history }) {  // eslint-disable-line
    },
  },

  effects: {
    *dummyEffect({ payload }, { put, call }) {  // eslint-disable-line
    },
  },

  reducers: {
    dummyReducer(state) {
      return state;
    },
  },
};
