import React, { Component } from 'react';
import { connect } from '@lattebank/moka';

@connect(() => ({
}))
export default class IndexPage extends Component {
  static propTypes = {
  };

  render() {
    const childId = this.props.params.childId || 'NULL';
    return (
      <h3>Hello, this is child page, I am child {childId}</h3>
    );
  }
}
