import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from '@lattebank/moka';
import { Breadcrumb, Card, Col, Row } from 'antd';
import Immutable from 'immutable';
import MovieTable from './MovieTable';


@connect(state => ({
  movie: state.movie,
  loading: state.loading.effects,
}))
export default class IndexPage extends Component {
  static propTypes = {
    movie: PropTypes.instanceOf(Immutable.Map),
    loading: PropTypes.object,
  };

  render() {
    const { movie, loading } = this.props;
    const moviesInTheater = movie.get('moviesInTheater').toJS();
    const moviesComingSoon = movie.get('moviesComingSoon').toJS();
    const comicMovies = movie.get('comicMovies').toJS();
    const scificMovies = movie.get('scificMovies').toJS();
    return (
      <div>
        <div className="ant-layout-breadcrumb">
          <Breadcrumb>
            <Breadcrumb.Item>TP320</Breadcrumb.Item>
            <Breadcrumb.Item>电影资讯</Breadcrumb.Item>
          </Breadcrumb>
        </div>
        <div className="ant-layout-container">
          <Row gutter={12}>
            <Col span={12}>
              <Card title="热映电影" bordered>
                <MovieTable loading={loading['movie/fetchInTheaterMovies']} dataSource={moviesInTheater} />
              </Card>
            </Col>
            <Col span={12}>
              <Card title="即将上映" bordered>
                <MovieTable loading={loading['movie/fetchComingSoonMovies']} dataSource={moviesComingSoon} />
              </Card>
            </Col>
          </Row>
          <Row gutter={12}>
            <Col span={12}>
              <Card title="喜剧电影" bordered>
                <MovieTable loading={loading['movie/fetchComicMovies']} dataSource={comicMovies} />
              </Card>
            </Col>
            <Col span={12}>
              <Card title="科幻电影" bordered>
                <MovieTable loading={loading['movie/fetchSciFicMovies']} dataSource={scificMovies} />
            </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
