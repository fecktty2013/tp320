import Immutable from 'immutable';
import * as service from './service';

export default {
  namespace: 'movie',

  state: Immutable.fromJS({
    moviesInTheater: [],
    moviesComingSoon: [],
    comicMovies: [],
    scificMovies: [],
  }),

  subscriptions: {
    setup({ history }) {
      return history.mapRouteToAction([
        { path: '/movies', action: 'fetchAllMovies' },
      ]);
    },
  },

  effects: {
    *fetchAllMovies({ payload }, { put }) {
      yield put({ type: 'fetchInTheaterMovies' });
      yield put({ type: 'fetchComingSoonMovies' });
      yield put({ type: 'fetchComicMovies' });
      yield put({ type: 'fetchSciFicMovies' });
    },
    *fetchInTheaterMovies(action, { put, call }) {
      const result = yield call(service.moviesInTheaters);
      yield put({ type: 'fetchInTheaterMoviesSuccess', payload: result.data });
    },
    *fetchComingSoonMovies(action, { put, call }) {
      const result = yield call(service.moviesComingSoon);
      yield put({ type: 'fetchComingSoonMoviesSuccess', payload: result.data });
    },
    *fetchComicMovies(action, { put, call }) {
      const result = yield call(service.comicMovies);
      yield put({ type: 'fetchComicMoviesSuccess', payload: result.data });
    },
    *fetchSciFicMovies(action, { put, call }) {
      const result = yield call(service.scificMovies);
      yield put({ type: 'fetchSciFicMoviesSuccess', payload: result.data });
    },
  },

  reducers: {
    fetchInTheaterMoviesSuccess(state, { payload }) {
      return state.update('moviesInTheater', () => Immutable.fromJS(payload.subjects));
    },
    fetchComingSoonMoviesSuccess(state, { payload }) {
      return state.update('moviesComingSoon', () => Immutable.fromJS(payload.subjects));
    },
    fetchComicMoviesSuccess(state, { payload }) {
      return state.update('comicMovies', () => Immutable.fromJS(payload.subjects));
    },
    fetchSciFicMoviesSuccess(state, { payload }) {
      return state.update('scificMovies', () => Immutable.fromJS(payload.subjects));
    },
  },
};
