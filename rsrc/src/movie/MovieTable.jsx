import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import style from './style.scss';

function renderMovieDetail(record) {
  function renderCasts() {
    return record.casts.map(cast => (
      <a key={cast.id} href={cast.alt}>{cast.name}</a>
    ));
  }

  function renderDirectors() {
    return record.directors.map(director => (
      <a key={director.id} href={director.alt}>{director.name}</a>
    ));
  }

  return (
    <div className={style.moviePanel}>
      <div className={style.left}>
        <img src={record.images.small} alt="" />
      </div>
      <div className={style.right}>
        <p className={style.directors}><span className={style.label}>导演:</span>{renderDirectors()}</p>
        <p className={style.casts}><span className={style.label}>演员:</span>{renderCasts()}</p>
      </div>
    </div>
  );
}

export default function MovieTable(props) {
  const columns = [
    {
      title: '电影名称',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: '类型',
      dataIndex: '',
      key: 'type',
      render: record => record.genres.join(' | '),
    },
    {
      title: '上映年份',
      dataIndex: 'year',
      key: 'year',
    },
    {
      title: '电影评分',
      dataIndex: '',
      key: 'rating',
      render: record => record.rating.average,
    },
  ];

  return (
    <Table
      rowKey={(record, index) => index}
      columns={columns}
      expandedRowRender={renderMovieDetail}
      loading={props.loading}
      dataSource={props.dataSource} />
  );
}

MovieTable.propTypes = {
  dataSource: PropTypes.array,
  loading: PropTypes.bool,
};
