import { $http } from '@lattebank/moka';

// const baseEndpoint = 'https://api.douban.com';
const baseEndpoint = `${head.CONTEXT_PATH}innersso-client/api`;

export function moviesInTheaters() {
  return $http.get(`${baseEndpoint}/v2/movie/in_theaters`);
}

export function moviesComingSoon() {
  return $http.get(`${baseEndpoint}/v2/movie/coming_soon`);
}

export function moviesByTag(tag) {
  return $http.get(`${baseEndpoint}/v2/movie/search`, { tag });
}

export function comicMovies() {
  return moviesByTag('喜剧');
}

export function scificMovies() {
  return moviesByTag('科幻');
}
